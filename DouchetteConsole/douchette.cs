using System;
using System.Collections.Generic;
using System.Data.SQLite;


namespace douchette{
    public class Douchette{

        const int max_char = 3; //max de caractere sur le 1er packets
        const int max_number = 4;   // max de caractere sur les deux autres packets
        const int max_packet = 3;   //nombre max de packets dans la code barre
        public string code_barre;   //code barre saisie a enregistrer dans la bdd
        public bool verif;  //boolean de verification de validité

        public void bip(bool verif){
            if(verif){
                Console.WriteLine("Bip");   //si verif est vrai on bip une fois
            }
            else{
                Console.WriteLine("Bip bip");   //si verif est false on bip deux fois
            }
        }

        public string scan(){
            code_barre = Console.ReadLine();    //saisie code barre
            return code_barre;
        }

        public List<string> split_paquet(string code_barre){

            List<string> cuted_code_barres = new List<string>(); //list des element du code barre

            String[] code_barre_split = new String[max_packet];
            code_barre_split = code_barre.Split("-");   //split des packets sur le - et ajout a un tableau de string
                        
            foreach(string cb in code_barre_split) 
                cuted_code_barres.Add(cb);  //ajout des packets dans le tableau de string a la liste
                verif = verification_packets(code_barre_split); //verif validité packets
            return cuted_code_barres;
        }

        public bool verification_packets(string[] cuted_code_barres){

            int temp;   //variable temporaire pour les tryparse pour la verification d'entier

            //verification du nombre de packet
            if(cuted_code_barres.Length != max_packet){
                Console.WriteLine("nombre de packet incorrect");
                return false;
            }

            if(cuted_code_barres[0].Length != max_char) //si taille premier packets != 3 -> false
            {
                Console.WriteLine("taille incorrect premier packet");
                return false;
            }            

            //verification taille des packets 2 et 3
            for(int no_packet = 1; no_packet <= 2; no_packet++){ 
                if(cuted_code_barres[no_packet].Length != max_number)
                {
                    Console.WriteLine("taille packet ");
                    Console.WriteLine(no_packet+1);
                    Console.WriteLine("incorrect");
                    return false;
                }
            }

            //verification si le premier packet est composé de char
            foreach(char val in cuted_code_barres[0]){
                if(char.IsLetter(val) == false){
                    Console.WriteLine("1er packet pas char");
                    return false;
                }
            }

            //verification si les 2 autres packet sont des int
            for(int no_packet = 1; no_packet <= 2; no_packet++){
                if(int.TryParse(cuted_code_barres[no_packet], out temp) == false){
                    Console.WriteLine(no_packet);
                    Console.WriteLine(" e packet pas entier");
                    return false;
                }
            }

            //si le code barre est valide on retourne true
            Console.WriteLine("code valide");
            return true;            
        }

        public SQLiteConnection connexion(){
            //initialisation d'une connection sql dans la variable con
            SQLiteConnection con = new SQLiteConnection("Data Source=C:/Users/basti/Desktop/2eme annee/Douchette/projet-douchette/Inventaie.db");
            return con;
        }

        public void save(SQLiteConnection con, string type, int annee, int reference){     

            con.Open(); //ouverture connection sql
            var cmd = new SQLiteCommand(con);

            //requete sql d'insertion avec données passer en parametre
            cmd.CommandText = "INSERT INTO reference(Type_de_produit, Année_de_production, Référence_de_produit) VALUES (@type,@annee,@ref)";
            cmd.Prepare();

            cmd.Parameters.AddWithValue("@type", type);
            cmd.Parameters.AddWithValue("@annee", annee);
            cmd.Parameters.AddWithValue("@ref", reference);
            cmd.ExecuteNonQuery();  //execution de la requete

            con.Close();    //fermeture connection bdd

        }
    }
}