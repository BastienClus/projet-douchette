﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;

namespace douchette
{
    class Program
    {
        static void Main(string[] args)
        {
            SQLiteConnection con = new SQLiteConnection();
            List<string> cuted_code_barre = new List<string>();
            Douchette Zebra_TC20 = new Douchette(); //douchette allumé
           

            while(Zebra_TC20.verif == false){
                
                Zebra_TC20.code_barre = Zebra_TC20.scan();  //saisie du code barre par la douchette
                cuted_code_barre = Zebra_TC20.split_paquet(Zebra_TC20.code_barre);  //split des données du code barre

                Zebra_TC20.bip(Zebra_TC20.verif);   //bip de verification de la douchette
                //sauvegarde des données du code barre dans des variable avec le bon type
                string type = cuted_code_barre[0];
                int annee = int.Parse(cuted_code_barre[1]);
                int reference = int.Parse(cuted_code_barre[2]);
                
                if(Zebra_TC20.verif == true){
                    //connection et sauvegarde des donnees dans la bdd
                    con = Zebra_TC20.connexion();
                    Zebra_TC20.save(con, type, annee ,reference);
                }
            }
            
        }
    }
}
